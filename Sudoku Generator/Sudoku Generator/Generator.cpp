#include "Generator.h"

#include <map>

Generator::Generator(Board board) : m_board(board) {}

void Generator::reduce(int difficulty)
{
	reduceLogical(difficulties[difficulty].first);
	if (difficulty > 0) {
		reduceRandom(difficulties[difficulty].second);
	}
}

void Generator::reduceLogical(int cutoff)
{
	std::vector<Cell*> cells = m_board.getUsedCells();
	std::random_shuffle(cells.begin(), cells.end());

	for (auto cell : cells) {
		int orig = cell->value;
		cell->value = 0;
		if (m_board.getNumberOfPossibilities(*cell) != 1) {
			cell->value = orig;
			--cutoff;
		}
		if (cutoff == 0) {
			break;
		}
	}
}

void Generator::reduceRandom(int cutoff)
{
	std::multimap<double, Cell*> sorted;
	for (auto cell : m_board.getUsedCells()) {
		sorted.insert({ m_board.getDensity(*cell), cell });
	}

	for (auto it = sorted.rbegin(); it != sorted.rend(); ++it) {

		Cell& cell = *it->second;
		int original = cell.value;
		std::vector<int> complement;
		for (int i = 1; i <= m_board.gridSize; ++i) {
			if (i != cell.value) {
				complement.push_back(i);
			}
		}

		bool ambigous = false;
		for (auto i : complement) {
			cell.value = i;
			Solver solver(m_board);
			if (solver.solve() && solver.board().isValid()) {
				cell.value = original;
				ambigous = true;
				break;
			}
		}

		if (!ambigous) {
			cell.value = 0;
			--cutoff;
		}

		if (cutoff == 0) {
			break;
		}
	}

}