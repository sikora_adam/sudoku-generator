#include <iostream>
#include <fstream>

#include "Board9x9.h"
#include "Solver.h"
#include "Generator.h"

int main()
{
	try {
		for (int i = 0; i < 1; ++i) {
			auto board = Board::fromFile();
			std::cout << board.print().c_str() << std::endl;
			board.shuffle(6846);
			std::cout << board.print().c_str() << std::endl;
			std::cout << board.isValid() << std::endl;
			Generator gen(board);
			gen.reduce(3);
			std::cout << gen.board().print().c_str() << std::endl;

			Solver solver(gen.board());
			std::cout << solver.solve() << std::endl;
			std::cout << (solver.board().isFilled() && solver.board().isValid()) << std::endl;

			std::ofstream file("Sudoku.txt");
			file << gen.board().print().c_str();
			file.close();
		}
	} catch (std::exception& e) {
		std::cout << e.what() << std::endl;
		std::cin.ignore();
		return 1;
	}
	std::cin.ignore();
	return 0;
}