#pragma once

#include <array>
#include <vector>
#include <bitset>

#include "Cell.h"

class BoardCube {
public:
	BoardCube();
	BoardCube(std::vector<int> numbers);
	static BoardCube fromFile(std::string filename = "BoardCubeBase.txt");
	std::string print();
	std::string html();
	bool isFilled();
	void shuffle(int iterations, uint64_t seed = std::numeric_limits<uint64_t>::max());


	static const int gridSize = 16;
	static const int groupSize = 4;
	static const int groupCount = 3;
	static const int size = 96;

	const std::vector<Cell*> getUnusedCells() {
		std::vector<Cell*> result;
		for (auto &i : m_cells) {
			if (i.value == 0) {
				result.emplace_back(&i);
			}
		}
		return result;
	}
	const std::vector<Cell*> getUsedCells() {
		std::vector<Cell*> result;
		for (auto &i : m_cells) {
			if (i.value != 0) {
				result.emplace_back(&i);
			}
		}
		return result;
	}
	bool isPossible(const Cell& cell, int x);
	std::bitset<gridSize> getPossibilities(const Cell& cell);
	int getNumberOfPossibilities(const Cell& cell);
	double getDensity(const Cell& cell);

	bool isValid();

private:
	std::vector<Cell> m_cells;

	using Group = std::array<int, gridSize>;
	std::vector<Group> m_boxes;
	std::vector<Group> m_stripes;

	void swapNumbers(int first, int second);
	void swapStripes(int first, int second);
};