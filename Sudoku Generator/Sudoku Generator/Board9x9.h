#pragma once

#include <array>
#include <vector>
#include <bitset>

#include "Cell.h"

class Board9x9 {
public:
	Board9x9();
	Board9x9(std::vector<int> numbers);
	static Board9x9 fromFile(std::string filename = "Board9x9Base.txt");
	std::string print();
	std::string html();
	bool isFilled();
	void shuffle(int iterations, uint64_t seed = std::numeric_limits<uint64_t>::max());


	static const int gridSize = 9;
	static const int groupSize = 3;
	static const int size = 81;

	const std::vector<Cell*> getUnusedCells() {
		std::vector<Cell*> result;
		for (auto &i : m_cells) {
			if (i.value == 0) {
				result.emplace_back(&i);
			}
		}
		return result;
	}
	const std::vector<Cell*> getUsedCells() {
		std::vector<Cell*> result;
		for (auto &i : m_cells) {
			if (i.value != 0) {
				result.emplace_back(&i);
			}
		}
		return result;
	}
	bool isPossible(const Cell& cell, int x);
	std::bitset<gridSize> getPossibilities(const Cell& cell);
	int getNumberOfPossibilities(const Cell& cell);
	double getDensity(const Cell& cell);

	bool isValid();

private:
	std::vector<Cell> m_cells;

	using Group = std::array<int, gridSize>;
	std::vector<Group> m_boxes;
	std::vector<Group> m_rows;
	std::vector<Group> m_cols;

	void swapNumbers(int first, int second);
	void swapRows(int first, int second, bool check = true);
	void swapCols(int first, int second, bool check = true);
	void swapRowsGroup(int first, int second);
	void swapColsGroup(int first, int second);
};