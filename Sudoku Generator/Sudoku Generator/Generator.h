#pragma once

#include "Solver.h"

class Generator {
public:
	Generator(Board board);
	void reduce(int difficulty);
	Board board() { return m_board; }

private:
	Board m_board;

	void reduceLogical(int cutoff);
	void reduceRandom(int cutoff);

	std::vector<std::pair<int, int>> difficulties = {
		{ Board::size / 2, 0 },
		{ Board::size, 5 },
		{ Board::size, 10 },
		{ Board::size, 20 }
	};
};