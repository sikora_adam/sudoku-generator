#include "Solver.h"
#include "algorithm"

Solver::Solver(Board board) : m_board(board), m_vacants(m_board.getUnusedCells()) {}

bool Solver::solve()
{
	int index = 0;
	while (index > -1 && index < m_vacants.size()) {
		Cell& current = *m_vacants[index];
		bool flag = false;
		int start = current.value + 1;
		current.value = 0;
		for (int x = start; x <= m_board.gridSize; ++x) {
			if (m_board.isPossible(current, x)) {
				current.value = x;
				flag = true;
				break;
			}
		}
		if (!flag) {
			current.value = 0;
			--index;
		} else {
			++index;
		}
	}
	if (m_vacants.size() == 0) {
		return false;
	} else {
		return index == m_vacants.size();
	}
}