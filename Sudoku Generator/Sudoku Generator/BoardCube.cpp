#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <chrono>
#include <set>

#include "BoardCube.h"

BoardCube::BoardCube()
{
	BoardCube(std::vector<int>(size, 0));
}

BoardCube::BoardCube(std::vector<int> numbers)
{
	if (numbers.size() != size) {
		throw std::exception("Incorrect initialization vector size");
	}

	m_cells = std::vector<Cell>(size, Cell());
	m_boxes = std::vector<Group>(6, Group());
	m_stripes = std::vector<Group>(groupCount * groupSize, Group());

	//  stripes
	//      12
	//   02 01 02 01
	//      12
	// cell.row is smaller number, col is bigger

	for (int i = 0; i < size; ++i) {
		m_cells[i].value = numbers[i];
		if (i < gridSize) {
			m_cells[i].box = 0;
			m_cells[i].row = groupSize + i % groupSize;
			m_cells[i].col = 2 * groupSize + i / groupSize;
		} else if (i < size - gridSize) {
			m_cells[i].box = 1 + ((i - gridSize) % gridSize) / groupSize;
			m_cells[i].row = i / gridSize - 1;
			switch ((i % gridSize) / groupSize) {
			case 0: {
				m_cells[i].col = 2 * groupSize + i % groupSize;
				break;
			}
			case 1: {
				m_cells[i].col = groupSize + i % groupSize;
				break;
			}
			case 2: {
				m_cells[i].col = 2 * groupSize + groupSize - i % groupSize - 1;
				break;
			}
			case 3: {
				m_cells[i].col = groupSize + groupSize - i % groupSize - 1;
				break;
			}
			}
		} else {
			m_cells[i].box = 5;
			m_cells[i].row = groupSize + i % groupSize;
			m_cells[i].col = 2 * groupSize + (size - 1 - i) / groupSize;
		}
	}

	for (int i = 0; i < gridSize; ++i) {
		m_boxes[0][i] = i;
		m_boxes[5][i] = size - gridSize + i;
		for (int j = 0; j < groupSize; ++j) {
			m_boxes[i / groupSize + 1][4 * j + i % groupSize] = (1 + j) * gridSize + i;
		}
	}

	for (int i = 0; i < gridSize; ++i) {
		for (int j = 0; j < groupSize; ++j) {
			m_stripes[j][i] = (1 + j) * gridSize + i;
		}
	}

	for (int i = 0; i < groupSize; ++i) {
		for (int j = 0; j < gridSize; ++j) {
			int value;
			switch (j / groupSize) {
			case 0: {
				value = groupSize * i + j % groupSize;
				break;
			}
			case 1: {
				value = (1 + j % groupSize) * gridSize + 11 - i;
				break;
			}
			case 2: {
				value = size - 1 - (groupSize * i + j % groupSize);
				break;
			}
			case 3: {
				value = size - ((2 + j % groupSize) * gridSize - i);
				break;
			}
			}
			m_stripes[2 * groupSize + i][j] = value;
		}
	}

	for (int i = 0; i < groupSize; ++i) {
		for (int j = 0; j < gridSize; ++j) {
			int value;
			switch (j / groupSize) {
			case 0: {
				value = groupSize * j + i;
				break;
			}
			case 1: {
				value = (1 + j % groupSize) * gridSize + groupSize + i;
				break;
			}
			case 2: {
				value = size - gridSize + groupSize * (j % groupSize) + i;
				break;
			}
			case 3: {
				value = size - ((1 + j % groupSize) * gridSize + i + 1);
				break;
			}
			}
			m_stripes[groupSize + i][j] = value;
		}
	}

	for (int i = 0; i < size; ++i) {
		bool isThere = false;
		for (int cell : m_boxes[m_cells[i].box]) {
			if (m_cells[i] == m_cells[cell]) {
				isThere = true;
			}
		}
		if (!isThere) {
			throw std::exception("Incorrect initialization");
		}
	}
	for (int i = 0; i < 6; ++i) {
		for (int cell : m_boxes[i]) {
			if (m_cells[cell].box != i) {
				throw std::exception("Incorrect initialization");
			}
		}
	}
	for (int i = 0; i < size; ++i) {
		bool isThere = false;
		for (int cell : m_stripes[m_cells[i].row]) {
			if (m_cells[i] == m_cells[cell]) {
				isThere = true;
			}
		}
		for (int cell : m_stripes[m_cells[i].col]) {
			if (m_cells[i] == m_cells[cell]) {
				isThere = true;
			}
		}
		if (!isThere) {
			throw std::exception("Incorrect initialization");
		}
	}
	for (int i = 0; i < 12; ++i) {
		for (int cell : m_stripes[i]) {
			if (m_cells[cell].row != i && m_cells[cell].col != i) {
				throw std::exception("Incorrect initialization");
			}
		}
	}
}

BoardCube BoardCube::fromFile(std::string filename)
{
	std::vector<int> numbers;

	std::ifstream file(filename);
	if (!file.good()) {
		throw std::exception("Unbable to open file");
	}

	while (!file.eof()) {
		int i;
		file >> i;
		numbers.push_back(i);
	}

	return BoardCube(numbers);
}

namespace {
	std::string printInternal(int value)
	{
		std::string result;
		if (value == 0) {
			result += "__|";
		}
		else {
			if (std::to_string(value).size() == 1) {
				result += ' ';
			}
			result += std::to_string(value);
			result += '|';
		}
		return result;
	}
}

std::string BoardCube::print()
{
	std::string result;
	for (int row = 0; row < groupSize; ++row) {
		result += "            |";
		for (int col = 0; col < groupSize; ++col) {
			int position = groupSize * row + col;
			result += printInternal(m_cells[position].value);
		}
		result += '\n';
	}

	for (int row = 0; row < groupSize; ++row) {
		result += "|";
		for (int col = 0; col < gridSize; ++col) {
			int position = gridSize + gridSize * row + col;
			result += printInternal(m_cells[position].value);
		}
		result += '\n';
	}

	for (int row = 0; row < groupSize; ++row) {
		result += "            |";
		for (int col = 0; col < groupSize; ++col) {
			int position = size - gridSize + groupSize * row + col;
			result += printInternal(m_cells[position].value);
		}
		result += '\n';
	}
	return result;
}

std::string BoardCube::html()
{
	// TODO rework
	std::string result = R"(
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>HTML Sudoku Board</title>
    
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    
    <style type="text/css">
    
      html, body {
        background-color: #FAFAFA
      }

      table {
        border: 2px solid #000000;
      }

      td {
        border: 1px solid #000000;
        text-align: center;
        vertical-align: middle;  
      }

      input {
        color: #000000;
        padding: 0;
        border: 0;
        text-align: center;
        width: 48px;
        height: 48px;
        font-size: 24px;
        background-color: #FFFFFF;
        outline: none;
      }

      input:disabled {
        background-color: #EEEEEE;
      }

      #cell-0,  #cell-1,  #cell-2  { border-top:    2px solid #000000; }
      #cell-2,  #cell-11, #cell-20 { border-right:  2px solid #000000; }
      #cell-18, #cell-19, #cell-20 { border-bottom: 2px solid #000000; }
      #cell-0,  #cell-9,  #cell-18 { border-left:   2px solid #000000; }

      #cell-3,  #cell-4,  #cell-5  { border-top:    2px solid #000000; }
      #cell-5,  #cell-14, #cell-23 { border-right:  2px solid #000000; }
      #cell-21, #cell-22, #cell-23 { border-bottom: 2px solid #000000; }
      #cell-3,  #cell-12, #cell-21 { border-left:   2px solid #000000; }

      #cell-6,  #cell-7,  #cell-8  { border-top:    2px solid #000000; }
      #cell-8,  #cell-17, #cell-26 { border-right:  2px solid #000000; }
      #cell-24, #cell-25, #cell-26 { border-bottom: 2px solid #000000; }
      #cell-6,  #cell-15, #cell-24 { border-left:   2px solid #000000; }

      #cell-27, #cell-28, #cell-29 { border-top:    2px solid #000000; }
      #cell-29, #cell-38, #cell-47 { border-right:  2px solid #000000; }
      #cell-45, #cell-46, #cell-47 { border-bottom: 2px solid #000000; }
      #cell-27, #cell-36, #cell-45 { border-left:   2px solid #000000; }

      #cell-30, #cell-31, #cell-32 { border-top:    2px solid #000000; }
      #cell-32, #cell-41, #cell-50 { border-right:  2px solid #000000; }
      #cell-48, #cell-49, #cell-50 { border-bottom: 2px solid #000000; }
      #cell-30, #cell-39, #cell-48 { border-left:   2px solid #000000; }

      #cell-33, #cell-34, #cell-35 { border-top:    2px solid #000000; }
      #cell-35, #cell-44, #cell-53 { border-right:  2px solid #000000; }
      #cell-51, #cell-52, #cell-53 { border-bottom: 2px solid #000000; }
      #cell-33, #cell-42, #cell-51 { border-left:   2px solid #000000; }

      #cell-54, #cell-55, #cell-56 { border-top:    2px solid #000000; }
      #cell-56, #cell-65, #cell-74 { border-right:  2px solid #000000; }
      #cell-72, #cell-73, #cell-74 { border-bottom: 2px solid #000000; }
      #cell-54, #cell-63, #cell-72 { border-left:   2px solid #000000; }

      #cell-57, #cell-58, #cell-59 { border-top:    2px solid #000000; }
      #cell-59, #cell-68, #cell-77 { border-right:  2px solid #000000; }
      #cell-75, #cell-76, #cell-77 { border-bottom: 2px solid #000000; }
      #cell-57, #cell-66, #cell-75 { border-left:   2px solid #000000; }

      #cell-60, #cell-61, #cell-62 { border-top:    2px solid #000000; }
      #cell-62, #cell-71, #cell-80 { border-right:  2px solid #000000; }
      #cell-78, #cell-79, #cell-80 { border-bottom: 2px solid #000000; }
      #cell-60, #cell-69, #cell-78 { border-left:   2px solid #000000; }

    </style>
  </head>
  <body>

    <div class="container">

      <table id="grid">

)";
	for (int i = 0; i < gridSize; ++i) {
		std::string row = "<tr>";
		for (int j = 0; j < gridSize; ++j) {
			int index = gridSize * i + j;
			int value = m_cells[index].value;
			R"(<td><input id="cell-66" type="text" value="4" disabled></td>)";
			row += R"(<td><input id="cell-)";
			row += std::to_string(index);
			row += R"(" type="text")";
			if (value != 0) {
				row += R"( value=")";
				row += std::to_string(value);
				row += R"(" disabled)";
			}
			row += "></td>";
		}
		row += "</tr>";
		result += row;
	}
	result += R"(
      </table>

    </div>

  </body>
</html>
)";
	return result;
}

bool BoardCube::isFilled()
{
	for (auto &cell : m_cells) {
		if (cell.value == 0) {
			return false;
		}
	}
	return true;
}

void BoardCube::shuffle(int iterations, uint64_t seed)
{
	if (seed == std::numeric_limits<uint64_t>::max()) {
		seed = std::chrono::steady_clock::now().time_since_epoch().count();
	}
	std::mt19937 generator(seed);
	std::uniform_int_distribution<int> what(0, 1);
	std::uniform_int_distribution<int> whatNumber(1, gridSize);
	std::uniform_int_distribution<int> whatGroup(0, groupCount - 1);
	std::uniform_int_distribution<int> whatWithinGroup(0, groupSize - 1);

	if (!isFilled()) {
		throw std::exception("Cannot shuffle partially filled board");
	}
	for (int i = 0; i < iterations; ++i) {
		int action = what(generator);
		int first, second;
		if (action < 1) {
			first = whatNumber(generator);
			do {
				second = whatNumber(generator);
			} while (second == first);
		} else {
			first = whatWithinGroup(generator);
			do {
				second = whatWithinGroup(generator);
			} while (second == first);
			int group = groupSize * whatGroup(generator);
			first += group;
			second += group;
		}

		switch (action) {
		case 0: {
			swapNumbers(first, second);
			break;
		}
		case 1: {
			swapStripes(first, second);
			break;
		}
		}
	}
}

void BoardCube::swapNumbers(int first, int second)
{
	if (first < 1 || first > gridSize || second < 1 || second > gridSize) {
		throw std::exception("Invalid numbers to swap");
	}
	for (auto& cell : m_cells) {
		if (cell.value == first) {
			cell.value = second;
		} else if (cell.value == second) {
			cell.value = first;
		}
	}
}

void BoardCube::swapStripes(int first, int second)
{
	if (first < 0 || first > groupSize * groupCount - 1 || second < 0 || second > groupSize * groupCount - 1 || first / groupSize != second / groupSize) {
		throw std::exception("Invalid cols to swap");
	}
	for (int i = 0; i < gridSize; ++i) {
		std::swap(m_cells[m_stripes[first][i]].value, m_cells[m_stripes[second][i]].value);
	}
}

bool BoardCube::isPossible(const Cell& cell, int x)
{
	if (cell.value != 0) {
		throw std::exception("Value already filled");
	}
	for (int i = 0; i < gridSize; ++i) {
		int val = m_cells[m_boxes[cell.box][i]].value;
		if (val == x) {
			return false;
		}
		val = m_cells[m_stripes[cell.row][i]].value;
		if (val == x) {
			return false;
		}
		val = m_cells[m_stripes[cell.col][i]].value;
		if (val == x) {
			return false;
		}
	}
	return true;
}

std::bitset<BoardCube::gridSize> BoardCube::getPossibilities(const Cell& cell)
{
	std::bitset<gridSize> possibilities;
	possibilities.set();

	if (cell.value != 0) {
		throw std::exception("Value already filled");
	}
	for (int i = 0; i < gridSize; ++i) {
		int val = m_cells[m_boxes[cell.box][i]].value;
		if (val != 0) {
			possibilities.reset(val - 1);
		}
		val = m_cells[m_stripes[cell.row][i]].value;
		if (val != 0) {
			possibilities.reset(val - 1);
		}
		val = m_cells[m_stripes[cell.col][i]].value;
		if (val != 0) {
			possibilities.reset(val - 1);
		}
	}
	return possibilities;
}

int BoardCube::getNumberOfPossibilities(const Cell& cell) {
	return getPossibilities(cell).count();
}

double BoardCube::getDensity(const Cell& cell)
{
	std::set<int> indices;
	for (int i = 0; i < gridSize; ++i) {
		indices.insert(m_boxes[cell.box][i]);
		indices.insert(m_stripes[cell.row][i]);
		indices.insert(m_stripes[cell.col][i]);
	}
	int count = 0;
	for (auto i : indices) {
		if (m_cells[i].value != 0) {
			++count;
		}
	}
	return count / 38.0; // 38 is number of unique neighbors
}

bool BoardCube::isValid()
{
	if (!isFilled()) {
		throw std::exception("Cannot validate partially filled board");
	}
	std::set<int> numbers;
	for (auto& group : { m_boxes, m_stripes }) {
		for (auto& stripe : group) {
			for (int i : stripe) {
				numbers.insert(m_cells[i].value);
			}
			if (numbers.size() != gridSize) {
				return false;
			}
			numbers.clear();
		}
	}
	return true;
}