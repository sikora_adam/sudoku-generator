#pragma once

struct Cell {
	int value;
	int box;
	int row;
	int col;
};

bool operator== (const Cell& f, const Cell& s);
bool operator!= (const Cell& f, const Cell& s);