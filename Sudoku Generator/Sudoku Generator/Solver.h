#pragma once

#include "Board.h"

class Solver {
public:
	Solver(Board board);
	bool solve();
	Board Solver::board() { return m_board; }

private:
	Board m_board;
	std::vector<Cell*> m_vacants;
};