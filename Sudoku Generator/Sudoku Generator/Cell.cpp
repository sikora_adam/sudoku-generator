#include "Cell.h"

bool operator== (const Cell& f, const Cell& s)
{
	return f.value == s.value && f.box == s.box && f.row == s.row && f.col == s.col;
}

bool operator!= (const Cell& f, const Cell& s)
{
	return f.value != s.value || f.box != s.box || f.row != s.row || f.col != s.col;
}